<?php

namespace Model;

class Vendor_Roles extends \Emagid\Core\Model {
  
    static $tablename = "vendor_roles";
    public static $fields = ['vendor_id','role_id'];

}

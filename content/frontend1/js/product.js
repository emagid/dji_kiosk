$(document).ready(function() {


	$(".product_customization_dropdown_wrapper").click(function(event){
      event.stopPropagation();
      $(".customization_dropdown_box").removeClass("active_dropdown_box");
      $(this).find(".customization_dropdown_box").addClass("active_dropdown_box");
    });

    
      $("body").click(function(){
        if($(".customization_dropdown_box").hasClass("active_dropdown_box")){
          $(".customization_dropdown_box").removeClass("active_dropdown_box");  
        }
      }); 

      $(".small_product_image").click(function(){
        $(".small_product_image").removeClass("active_product_image");
        var $product_nav_image_clicked = $(this);
        var $large_product_image = $("#large_product_image");
        $product_nav_image_clicked.addClass("active_product_image");
        var nav_image_url = $product_nav_image_clicked.attr("data-rollover_product_image");
        $large_product_image.find("img").attr("src",nav_image_url);
        $large_product_image.css({"background-image":"url('"+nav_image_url+"')"});
      });

    $(".customization_dropdown_box li").click(function(){
    	event.stopPropagation();
      $(".customization_dropdown_box").removeClass("active_dropdown_box");
      $(this).closest(".product_customization_dropdown_wrapper").find("li.selected").removeClass("selected");
      var selected_customization = $(this).text();
      $(this).addClass("selected");
      $(this).closest(".product_customization_dropdown_wrapper").find(".selected_customization").text(selected_customization);
    });

    $(".product_detail_tab").click(function(){
      $(".product_detail_tab").removeClass("product_detail_tab_active");
      $(this).addClass("product_detail_tab_active");
      var tab_selected_type = $(this).attr("data-product_tab");
      $(".product_tab_content").removeClass("active_product_tab_content");
      $(".product_tab_content[data-product_tab_content='"+tab_selected_type+"']").addClass("active_product_tab_content");
    });


	$('.recommended_products_slick_wrapper').slick({
      dots: false,
      infinite: true,
      pagination:false,
      slidesToShow: 4,
      touchThreshold:20,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 690,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }

      ]
    }); 
});